package es.cguijarro.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table (name="Usuarios")
@Entity
public class Usuario implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	//Atributos
	
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column
	private String name;
	@Column
	private String login;
	@Column
	private String pw;
	@Column
	private int movil;
	@Column
	private String email;
	@Column
	private Byte acceso;
	//@ManyToOne
	//private Empresa empresa;
	
	//Constructores
	
	public Usuario(Long id, String name, String login, String pw, int movil, String email, Byte acceso) {
		super();
		this.id = id;
		this.name = name;
		this.login=login;
		this.pw = pw;
		this.movil=movil;
		this.email=email;
		this.acceso=acceso;
	}
	
	public Usuario() {
		
	}
	
	//Getters y setters
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public int getMovil() {
		return movil;
	}

	public void setMovil(int movil) {
		this.movil = movil;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Byte getAcceso() {
		return acceso;
	}

	public void setAcceso(Byte acceso) {
		this.acceso = acceso;
	}

	//Métodos
	
	@Override
	public String toString() {
		return "Usuario [name=" + name + ", pw=" + pw + ", id=" + id + "]";
	}
	
}
