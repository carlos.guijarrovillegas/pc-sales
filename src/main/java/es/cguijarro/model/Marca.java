package es.cguijarro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table (name="Marcas")
@Entity
public class Marca {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column
	private String nombre;
	@Column
	private String pais;
	@Column
	private Byte fiabilidad;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public byte getFiabilidad() {
		return fiabilidad;
	}
	
	public void setFiabilidad(byte fiabilidad) {
		this.fiabilidad = fiabilidad;
	}
	public Marca(Long id, String nombre, String pais, byte fiabilidad) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.pais = pais;
		this.fiabilidad = fiabilidad;
	}
	
	public Marca() {
	}
	
	
}
