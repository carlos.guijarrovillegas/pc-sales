package es.cguijarro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table (name="Ventas")
@Entity
public class Venta {
	//Atributos
	
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@ManyToOne
	private Producto producto;
	@ManyToOne
	private Usuario usuario;
	
	//Constructores
	
	public Venta(Long id, Producto producto, Usuario usuario) {
		super();
		this.id = id;
		this.producto = producto;
		this.usuario = usuario;
	}
	
	public Venta() {
	}
	
	//Getters y setters

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
}
