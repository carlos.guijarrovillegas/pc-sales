package es.cguijarro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table (name="Productos")
@Entity
public class Producto {
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@ManyToOne
	private Marca marca;
	@Column
	private String nombre;
	@Column
	private Double precio;
	@ManyToOne
	private Empresa empresa;
	@Column
	private int stock;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Marca getMarca() {
		return marca;
	}
	public void setMarca(Marca marca) {
		this.marca = marca;
	}
	public Double getPrecio() {
		return precio;
	}
	public void setPrecio(Double precio) {
		this.precio = precio;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock=stock;
	}
	public void setEmpresa(Empresa empresa) {
		this.empresa=empresa;
	}
	public Empresa getEmpresa() {
		return empresa;
	}
	
	
	public Producto(Long id, Marca marca, String nombre, Double precio, int stock, Empresa empresa) {
		super();
		this.id = id;
		this.marca = marca;
		this.nombre = nombre;
		this.precio=precio;
		this.stock=stock;
		this.empresa=empresa;
	}
	
	public Producto() {
	}
	
}
