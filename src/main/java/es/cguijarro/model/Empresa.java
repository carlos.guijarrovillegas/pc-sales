package es.cguijarro.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Table (name="Empresas")
@Entity
public class Empresa{
	//Atributos
	
	@Id
	@GeneratedValue
	@Column
	private Long id;
	@Column
	private String nombre;
	@Column
	private String provincia;
	@Column
	private String web;
	
	//Constructores
	
	public Empresa(Long id, String nombre, String provincia, String web) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.provincia = provincia;
		this.web = web;
	}
	
	public Empresa() {
	}
	
	//Getters y setters
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getProvincia() {
		return provincia;
	}
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	public String getWeb() {
		return web;
	}
	public void setWeb(String web) {
		this.web = web;
	}
	
	
	
}


