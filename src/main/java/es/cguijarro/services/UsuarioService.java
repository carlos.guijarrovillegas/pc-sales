package es.cguijarro.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cguijarro.model.Usuario;
import es.cguijarro.repo.UsuarioRepository;

@Service
public class UsuarioService {
	@Autowired
	private UsuarioRepository ur;
	
	public Usuario findByIdUsuario(Long id) {
		return ur.findById(id).orElse(null);
	}
	
	public Usuario findByLogin(String login) {
		return ur.findByLogin(login);
	}
	
	public Usuario save(Usuario usuario) {
		return ur.save(usuario);
	}
	
	public List<Usuario> findAll(){
		return ur.findAll();
	}
	
	
	public void delete(Long id) {
		ur.deleteById(id);
	}
	
}
