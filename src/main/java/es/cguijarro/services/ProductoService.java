package es.cguijarro.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cguijarro.model.Empresa;
import es.cguijarro.model.Marca;
import es.cguijarro.model.Producto;
import es.cguijarro.repo.ProductoRepository;

@Service
public class ProductoService {
	@Autowired
	private ProductoRepository pr;
	
	public Producto findByIdProducto(Long id) {
		return pr.findById(id).orElse(null);
	}
	
	public Producto findByNombre(String nombre) {
		return pr.findByNombre(nombre);
	}
	
	public Producto findByMarca(Marca marca) {
		return pr.findByMarca(marca.getNombre());
	}
	
	public Producto findByEmpresa(Empresa empresa) {
		return pr.findByEmpresa(empresa.getNombre());
	}
	
	public Producto save(Producto producto) {
		return pr.save(producto);
	}
	
	public List<Producto> findAll(){
		return pr.findAll();
	}
	
	
	public void delete(Long id) {
		pr.deleteById(id);
	}
	
}