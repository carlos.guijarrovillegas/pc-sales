package es.cguijarro.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cguijarro.model.Marca;
import es.cguijarro.repo.MarcaRepository;

@Service
public class MarcaService {
	@Autowired
	private MarcaRepository mr;
	
	public Marca findByIdMarca(Long id) {
		return mr.findById(id).orElse(null);
	}
	
	public Marca findByNombre(String nombre) {
		return mr.findByNombre(nombre);
	}
	
	public Marca save(Marca marca) {
		return mr.save(marca);
	}
	
	public List<Marca> findAll(){
		return mr.findAll();
	}
	
	
	public void delete(Long id) {
		mr.deleteById(id);
	}
	
}
