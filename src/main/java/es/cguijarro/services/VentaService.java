package es.cguijarro.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cguijarro.model.Venta;
import es.cguijarro.repo.VentaRepository;

@Service
public class VentaService {
	@Autowired
	private VentaRepository vr;
	
	public Venta findByIdVenta(Long id) {
		return vr.findById(id).orElse(null);
	}
	
	public Venta save(Venta venta) {
		return vr.save(venta);
	}
	
	public List<Venta> findAll(){
		return vr.findAll();
	}
	
	public void delete(Long id) {
		vr.deleteById(id);
	}
	
}