package es.cguijarro.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.cguijarro.model.Empresa;
import es.cguijarro.repo.EmpresaRepository;

@Service
public class EmpresaService {
	@Autowired
	private EmpresaRepository me;
	
	public Empresa findByIdEmpresa(Long id) {
		return me.findById(id).orElse(null);
	}
	
	public Empresa findByNombre(String nombre) {
		return me.findByNombre(nombre);
	}
	
	public Empresa save(Empresa empresa) {
		return me.save(empresa);
	}
	
	public List<Empresa> findAll(){
		return me.findAll();
	}
	
	public void delete(Long id) {
		me.deleteById(id);
	}
	
}
