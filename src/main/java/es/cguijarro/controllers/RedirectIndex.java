package es.cguijarro.controllers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import es.cguijarro.model.Empresa;
import es.cguijarro.model.Marca;
import es.cguijarro.model.Producto;
import es.cguijarro.model.Usuario;
import es.cguijarro.model.Venta;
import es.cguijarro.repo.ConfirmationTokenRepository;
import es.cguijarro.repo.UsuarioRepository;
import es.cguijarro.security.ConfirmationToken;
import es.cguijarro.services.EmpresaService;
import es.cguijarro.services.MarcaService;
import es.cguijarro.services.ProductoService;
import es.cguijarro.services.UsuarioService;
import es.cguijarro.services.VentaService;
 
@Controller
public class RedirectIndex {
	
	//DB CONNECTION
	
	String driverName = "com.mysql.jdbc.Driver";
	String connectionUrl = "jdbc:mysql://localhost:3306/prueba?serverTimezone=UTC";
	String dbName = "prueba";
	String userId = "root";
	String password = "TFG_cgv@$%hf8u82DHGE";
	
	//END OF DB CONNECTION
	
	boolean triedAccessWithNoLogin;
	boolean wrongPassword;
	String key="A@fk552Ddm*188aLK";

	
	@Autowired private UsuarioService usuService;
	@Autowired private MarcaService marcaService;
	@Autowired private EmpresaService empService;
	@Autowired private ProductoService proService;
	@Autowired private VentaService venService;

	
    @RequestMapping("/Logout")
	public void Logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession sesion = request.getSession();
		triedAccessWithNoLogin=false;
		wrongPassword=false;
		sesion.setAttribute("Usuario",null);
		sesion.setAttribute("username", null);
		response.sendRedirect("index");
	}
    
    public void CleanSearch(HttpServletRequest request) {
    	HttpSession sesion = request.getSession();
    	sesion.setAttribute("busqueda", "");
    }
	
	public void LoginChecker(HttpServletRequest request) {
		HttpSession sesion = request.getSession();
		triedAccessWithNoLogin=false;
		wrongPassword=false;
    	if(sesion.getAttribute("username") == null)
    		triedAccessWithNoLogin=true;
    	sesion.setAttribute("AccessWithNoLogin",triedAccessWithNoLogin);
    	sesion.setAttribute("WrongPassword",wrongPassword);
	}
	
	public void EncryptPassword(Usuario usuario, HttpServletRequest request) {
		
		//QUERY SPACE
    	
    	try {
			Class.forName(driverName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		String sql="";
		String inputEncryptedPw="";
		
		try {
			connection = DriverManager.getConnection(connectionUrl, userId, password);
			statement = connection.createStatement();
			
			sql = "SELECT TO_BASE64(AES_ENCRYPT(\""+request.getParameter("inputPw") +"\",\""+ key +"\")) AS col;";
			resultSet = statement.executeQuery(sql);
			while(resultSet.next()) {
				//inputEncryptedPw = resultSet.getString("TO_BASE64(AES_ENCRYPT(\""+request.getParameter("inputPw") +"\",\""+ key +"\"))");
				inputEncryptedPw = resultSet.getString("col");
			}
			
			connection = DriverManager.getConnection(connectionUrl, userId, password);
			statement = connection.createStatement();
			sql = "UPDATE usuarios SET pw = '"+inputEncryptedPw+"' WHERE id = "+usuario.getId();
			statement.executeUpdate(sql);
			System.out.println(sql);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
    	//END OF QUERY SPACE
		
	}
	
	public boolean ProductoCreationOrModification(HttpServletRequest request, boolean isEdit) {
		HttpSession sesion = request.getSession();
		Producto producto;
		if(isEdit) {
			Long productoId = (Long)sesion.getAttribute("editId");
			producto = proService.findByIdProducto(productoId);
		}
		else
			producto = new Producto();
        	
    	if(IsValidProducto(request,producto,isEdit)) {
    		proService.save(producto);
            sesion.setAttribute("Success", true);
            return true;
    	}else {
    		sesion.setAttribute("WentWrong",true);
    		return false;
    	}
	}
	
	public boolean EmpresaCreationOrModification(HttpServletRequest request, boolean isEdit) {
		HttpSession sesion = request.getSession();
		Empresa empresa;
		if(isEdit) {
			Long empresaId = (Long)sesion.getAttribute("editId");
			empresa = empService.findByIdEmpresa(empresaId);
		}
		else
			empresa = new Empresa();
        	
    	if(IsValidEmpresa(request,empresa,isEdit)) {
    		empService.save(empresa);
            sesion.setAttribute("Success", true);
            return true;
    	}else {
    		sesion.setAttribute("WentWrong",true);
    		return false;
    	}
	}
	
	public boolean MarcaCreationOrModification(HttpServletRequest request, boolean isEdit) {
		HttpSession sesion = request.getSession();
		Marca marca;
		if(isEdit) {
			Long marcaId = (Long)sesion.getAttribute("editId");
        	marca = marcaService.findByIdMarca(marcaId);
		}
		else
			marca = new Marca();
        	
    	if(IsValidMarca(request,marca,isEdit)) {
            marcaService.save(marca);
            sesion.setAttribute("Success", true);
            return true;
    	}else {
    		sesion.setAttribute("WentWrong",true);
    		return false;
    	}
	}
	
	public boolean UserCreationOrModification(HttpServletRequest request, boolean isEdit, boolean isRegister) {
		HttpSession sesion = request.getSession();
		Usuario usuario;
		if(isEdit) {
			Long usuarioId = (Long)sesion.getAttribute("editId");
        	usuario = usuService.findByIdUsuario(usuarioId);
		}
		else
			usuario = new Usuario();
        	
    	if(IsValidUser(request,usuario,isEdit,isRegister)) {
            usuService.save(usuario);
            if(!isEdit)
            	EncryptPassword(usuario,request);
            sesion.setAttribute("Success", true);
            return true;
    	}else {
    		sesion.setAttribute("WentWrong",true);
    		return false;
    	}
	}
	
	public boolean IsValidProducto(HttpServletRequest request, Producto producto, boolean isEdit) {
		HttpSession sesion = request.getSession();
		boolean allRight = true;
		
		String nombreProducto = request.getParameter("inputNombre");
		Marca marcaProducto;
		try {
			marcaProducto = marcaService.findByIdMarca(Long.parseLong(request.getParameter("inputMarca")));
			if(marcaProducto==null) throw new Exception();
			producto.setMarca(marcaProducto);
		}catch(Exception e) {
			sesion.setAttribute("WrongMarca", true);
        	allRight=false;
		}
		
		Empresa empresaProducto;
		try {
			empresaProducto = empService.findByIdEmpresa(Long.parseLong(request.getParameter("inputEmpresa")));
			if(empresaProducto==null) throw new Exception();
			producto.setEmpresa(empresaProducto);
		}catch(Exception e) {
			sesion.setAttribute("WrongEmpresa", true);
        	allRight=false;
		}
	 	
		producto.setNombre(nombreProducto);
		
		try {
			producto.setPrecio(Double.parseDouble(request.getParameter("inputPrecio")));
	    	
	    	if(producto.getPrecio() < 0.01 || producto.getPrecio() > 5000) {
	    		sesion.setAttribute("PriceTooLowOrHigh",true);
	    		allRight=false;
	    	}
		}catch(Exception e) {
			sesion.setAttribute("WrongPrecio", true);
        	allRight=false;
		}
		try {
			producto.setStock(Integer.parseInt(request.getParameter("inputStock")));
	    	
	    	if(producto.getStock() < 0 || producto.getStock() > 100) {
	    		sesion.setAttribute("StockTooLowOrHigh",true);
	    		allRight=false;
	    	}
		}catch(Exception e) {
			sesion.setAttribute("WrongStock",true);
    		allRight=false;
		}
		
    	
    	if(isEdit) {
    		if(!request.getParameter("inputNombre").equals(sesion.getAttribute("editNombre")) && proService.findByNombre(request.getParameter("inputNombre")) != null){
    			sesion.setAttribute("UsedNombre", true);
    			sesion.setAttribute("UsedNombre", request.getParameter("inputNombre"));
            	allRight=false;
        	}
    		
    	}else {
    		if(proService.findByNombre(request.getParameter("inputNombre")) != null){
    			sesion.setAttribute("UsedNombre", true);
        		sesion.setAttribute("UsedNombreString", request.getParameter("inputNombre"));
            	allRight=false;
        	}
    	}
    	
    	if(producto.getNombre().length() > 50) {
    		sesion.setAttribute("NombreTooLong",true);
    		allRight=false;
    	}
    	
    	if(allRight)
    		return true;
    	else return false;
	}
	
	public boolean IsValidEmpresa(HttpServletRequest request, Empresa empresa, boolean isEdit) {
		HttpSession sesion = request.getSession();
		boolean allRight = true;
		empresa.setNombre(request.getParameter("inputNombre"));
		empresa.setProvincia(request.getParameter("inputProvincia"));
		empresa.setWeb(request.getParameter("inputWeb"));
    	
    	if(isEdit) {
    		if(!request.getParameter("inputNombre").equals(sesion.getAttribute("editNombre")) && empService.findByNombre(request.getParameter("inputNombre")) != null){
    			sesion.setAttribute("UsedNombre", true);
    			sesion.setAttribute("UsedNombre", request.getParameter("inputNombre"));
            	allRight=false;
        	}
    	}else {
    		if(empService.findByNombre(request.getParameter("inputNombre")) != null){
    			sesion.setAttribute("UsedNombre", true);
        		sesion.setAttribute("UsedNombreString", request.getParameter("inputNombre"));
            	allRight=false;
        	}
    	}
    	if(empresa.getNombre().length() > 30) {
    		sesion.setAttribute("NombreTooLong",true);
    		allRight=false;
    	}
    	if(empresa.getProvincia().length() > 30) {
    		sesion.setAttribute("ProvinciaTooLong",true);
    		allRight=false;
    	}
    	if(empresa.getWeb().split("\\.").length < 2) {
    		sesion.setAttribute("WrongWeb",true);
    		allRight=false;
    	}
    	
    	if(allRight)
    		return true;
    	else return false;
	}
	
	public boolean IsValidMarca(HttpServletRequest request, Marca marca, boolean isEdit) {
		HttpSession sesion = request.getSession();
		boolean allRight = true;
		marca.setNombre(request.getParameter("inputNombre"));
    	marca.setPais(request.getParameter("inputPais"));
    	marca.setFiabilidad(Byte.parseByte(request.getParameter("inputFiabilidad")));
    	
    	if(isEdit) {
    		if(!request.getParameter("inputNombre").equals(sesion.getAttribute("editNombre")) && marcaService.findByNombre(request.getParameter("inputNombre")) != null){
    			sesion.setAttribute("UsedNombre", true);
    			sesion.setAttribute("UsedNombre", request.getParameter("inputNombre"));
            	allRight=false;
        	}
    	}else {
    		if(marcaService.findByNombre(request.getParameter("inputNombre")) != null){
    			sesion.setAttribute("UsedNombre", true);
        		sesion.setAttribute("UsedNombreString", request.getParameter("inputNombre"));
            	allRight=false;
        	}
    	}
    	if(marca.getNombre().length() > 30) {
    		sesion.setAttribute("NombreTooLong",true);
    		allRight=false;
    	}
    	if(marca.getPais().length() > 30) {
    		sesion.setAttribute("PaisTooLong",true);
    		allRight=false;
    	}
    	if(marca.getFiabilidad() < 0 ||  marca.getFiabilidad() > 2) {
    		sesion.setAttribute("WrongFiabilidad",true);
    		allRight=false;
    	}
    	
    	if(allRight)
    		return true;
    	else return false;
	}
	
	public boolean IsValidUser(HttpServletRequest request, Usuario usuario, boolean isEdit, boolean isRegister) {
		HttpSession sesion = request.getSession();
		boolean allRight = true;
		usuario.setLogin(request.getParameter("inputLogin"));
    	usuario.setName(request.getParameter("inputName"));
    	if(!isEdit)
    		usuario.setPw(request.getParameter("inputPw"));
    	usuario.setEmail(request.getParameter("inputEmail"));
    	try {
    		usuario.setMovil(Integer.parseInt(request.getParameter("inputTlf")));
    	}
    	catch(Exception e) {
    		sesion.setAttribute("WrongTlfNumber",true);
    		allRight=false;
    	}
    	if(isRegister)
    		usuario.setAcceso(Byte.parseByte("0"));
    	else usuario.setAcceso(Byte.parseByte(request.getParameter("inputAcceso")));
    	if(isEdit) {
    		if(!request.getParameter("inputLogin").equals(sesion.getAttribute("editLogin")) && usuService.findByLogin(request.getParameter("inputLogin")) != null){
    			sesion.setAttribute("UsedLogin", true);
    			sesion.setAttribute("UsedLoginString", request.getParameter("inputLogin"));
            	allRight=false;
        	}
    	}else {
    		if(usuService.findByLogin(request.getParameter("inputLogin")) != null){
    			sesion.setAttribute("UsedLogin", true);
        		sesion.setAttribute("UsedLoginString", request.getParameter("inputLogin"));
            	allRight=false;
        	}
    	}
    	if(usuario.getLogin().length() > 10) {
    		sesion.setAttribute("LoginTooLong",true);
    		allRight=false;
    	}
    	if(usuario.getName().length() > 50) {
    		sesion.setAttribute("NameTooLong",true);
    		allRight=false;
    	}
    	if(!isEdit) {
    		if(/*usuario.getPw().length() > 30 ||*/ usuario.getPw().length() < 6) {
        		sesion.setAttribute("WrongPasswordSize",true);
        		allRight=false;
        	}
        	if(!request.getParameter("inputPw").equals(request.getParameter("inputPwRepeat"))) {
        		sesion.setAttribute("PasswordDoesntMatch",true);
        		allRight=false;
        	}
    	}
    	if(usuario.getEmail().split("@").length != 2 || usuario.getEmail().split("\\.").length < 2) {
    		sesion.setAttribute("WrongEmailSyntax",true);
    		allRight=false;
    	}
    	if(usuario.getMovil() < 600000000 || usuario.getMovil() > 999999999 ) {
    		sesion.setAttribute("WrongTlfNumber",true);
    		allRight=false;
    	}
    	if(usuario.getAcceso() < 0 ||  usuario.getAcceso() > 2) {
    		sesion.setAttribute("WrongAccess",true);
    		allRight=false;
    	}
    	
    	if(allRight)
    		return true;
    	else return false;
	}
 
    @RequestMapping("/index")
    public String index(HttpServletRequest request) {
    	HttpSession sesion = request.getSession();
    	sesion.setAttribute("AccessWithNoLogin",triedAccessWithNoLogin);
    	sesion.setAttribute("WrongPassword",wrongPassword);
        return "index";
    }
    
    @RequestMapping("/inicio")
    public String inicio(HttpServletRequest request) {
    	LoginChecker(request);
        return "inicio";
    }
    
    @RequestMapping("/productos")
    public String productos(HttpServletRequest request) {
    	LoginChecker(request);
        return "productos";
    }
    
    @RequestMapping("/marcas")
    public String marcas(HttpServletRequest request) {
    	LoginChecker(request);
        return "marcas";
    }
    
    @RequestMapping("/empresas")
    public String empresas(HttpServletRequest request) {
    	LoginChecker(request);
        return "empresas";
    }
    
    @RequestMapping("/usuarios")
    public String usuarios(HttpServletRequest request) {
    	LoginChecker(request);
    	CleanSearch(request);
        return "usuarios";
    }
    
    @RequestMapping("/ventas")
    public String ventas(HttpServletRequest request) {
    	LoginChecker(request);
        return "ventas";
    }
    
    @RequestMapping("/noPermission")
    public String noPermission(HttpServletRequest request) {
    	LoginChecker(request);
        return "noPermission";
    }
    
    @RequestMapping("/CrearProducto")
    public String CrearProducto(HttpServletRequest request) {
    	LoginChecker(request);
        return "crearProducto";
    }
    
    @RequestMapping("/CrearEmpresa")
    public String CrearEmpresa(HttpServletRequest request) {
    	LoginChecker(request);
        return "crearEmpresa";
    }
    
    @RequestMapping("/CrearMarca")
    public String CrearMarca(HttpServletRequest request) {
    	LoginChecker(request);
        return "crearMarca";
    }
    
    @RequestMapping("/CrearUsuario")
    public String CrearUsuario(HttpServletRequest request) {
    	LoginChecker(request);
        return "crearUsuario";
    }
    
    @RequestMapping("/EditarProducto")
    public String EditarProducto(HttpServletRequest request) {
    	HttpSession sesion = request.getSession();
    	if(request.getParameter("inputIdEdit")!=null) {
    		Long productoId = Long.parseLong(request.getParameter("inputIdEdit"));
    		Producto producto = proService.findByIdProducto(productoId);
        	
        	sesion.setAttribute("editId", producto.getId());
        	sesion.setAttribute("editMarcaId", producto.getMarca().getId());
        	sesion.setAttribute("editNombre", producto.getNombre());
        	sesion.setAttribute("editPrecio", producto.getPrecio());
        	sesion.setAttribute("editStock", producto.getStock());
        	sesion.setAttribute("editEmpresaId", producto.getEmpresa().getId());
        	
        	return "editarProducto";
    	}else {
    		sesion.setAttribute("EditNull", true);
    		return "index";
    	}
    	
    }
    
    @RequestMapping("/EditarEmpresa")
    public String EditarEmpresa(HttpServletRequest request) {
    	HttpSession sesion = request.getSession();
    	if(request.getParameter("inputIdEdit")!=null) {
    		Long empresaId = Long.parseLong(request.getParameter("inputIdEdit"));
    		Empresa empresa = empService.findByIdEmpresa(empresaId);
        	
        	sesion.setAttribute("editId", empresa.getId());
        	sesion.setAttribute("editNombre", empresa.getNombre());
        	sesion.setAttribute("editProvincia", empresa.getProvincia());
        	sesion.setAttribute("editWeb", empresa.getWeb());
        	
        	return "editarEmpresa";
    	}else {
    		sesion.setAttribute("EditNull", true);
    		return "index";
    	}
    	
    }
    
    @RequestMapping("/EditarMarca")
    public String EditarMarca(HttpServletRequest request) {
    	HttpSession sesion = request.getSession();
    	if(request.getParameter("inputIdEdit")!=null) {
    		Long marcaId = Long.parseLong(request.getParameter("inputIdEdit"));
        	Marca marca = marcaService.findByIdMarca(marcaId);
        	
        	sesion.setAttribute("editId", marca.getId());
        	sesion.setAttribute("editNombre", marca.getNombre());
        	sesion.setAttribute("editPais", marca.getPais());
        	sesion.setAttribute("editFiabilidad", marca.getFiabilidad());
        	
        	return "editarMarca";
    	}else {
    		sesion.setAttribute("EditNull", true);
    		return "index";
    	}
    	
    }
    
    @RequestMapping("/EditarUsuario")
    public String EditarUsuario(HttpServletRequest request) {
    	HttpSession sesion = request.getSession();
    	if(request.getParameter("inputIdEdit")!=null) {
    		Long usuarioId = Long.parseLong(request.getParameter("inputIdEdit"));
        	Usuario usuario = usuService.findByIdUsuario(usuarioId);
        	
        	sesion.setAttribute("editId", usuario.getId());
        	sesion.setAttribute("editLogin", usuario.getLogin());
        	sesion.setAttribute("editName", usuario.getName());
        	sesion.setAttribute("editEmail", usuario.getEmail());
        	sesion.setAttribute("editMovil", usuario.getMovil());
        	sesion.setAttribute("editAcceso", usuario.getAcceso());
        	
        	return "editarUsuario";
    	}else {
    		sesion.setAttribute("EditNull", true);
    		return "index";
    	}
    	
    }
    
    public void Search(HttpServletRequest request) {
    	HttpSession sesion = request.getSession();
    	String busqueda = request.getParameter("inputSearch");
    	sesion.setAttribute("busqueda",busqueda );
    }
    
    @RequestMapping("/SearchVenta")
    public String SearchVenta(HttpServletRequest request) {
    	Search(request);
    	return "ventas";
    }
    
    @RequestMapping("/SearchProducto")
    public String SearchProducto(HttpServletRequest request) {
    	Search(request);
    	return "productos";
    }
    
    @RequestMapping("/SearchEmpresa")
    public String SearchEmpresa(HttpServletRequest request) {
    	Search(request);
    	return "empresas";
    }
    
    @RequestMapping("/SearchMarca")
    public String SearchMarca(HttpServletRequest request) {
    	Search(request);
    	return "marcas";
    }
    
    @RequestMapping("/SearchUser")
    public String SearchUser(HttpServletRequest request) {
    	Search(request);
    	return "usuarios";
    }
    
    @RequestMapping("/ComprarProducto")
    public String ComprarProducto(HttpServletRequest request) {
    	HttpSession sesion = request.getSession();
    	Long productoId = Long.parseLong(request.getParameter("inputIdComprar"));
    	Long usuarioId = (Long)sesion.getAttribute("Id");
    	Usuario usuario = usuService.findByIdUsuario(usuarioId);
    	Producto producto = proService.findByIdProducto(productoId);
    	
		Venta venta = new Venta();
		venta.setUsuario(usuario);
		venta.setProducto(producto);
		if(producto.getStock() > 0) {
			producto.setStock(producto.getStock()-1);
			proService.save(producto);
			venService.save(venta);
			sesion.setAttribute("SuccessBuy", true);
	    	sesion.setAttribute("BuyMarcaName", producto.getMarca().getNombre());
	    	sesion.setAttribute("BuyProductoName", producto.getNombre());
	    	sesion.setAttribute("BuyPrecio", producto.getPrecio());
		}else sesion.setAttribute("SuccessBuy", false);
		
    	
    	return "productos";
    }
    
    @RequestMapping("/TramitarDevolucion")
    public String TramitarDevolucion(HttpServletRequest request) {
    	Long ventaId = Long.parseLong(request.getParameter("inputId"));
    	HttpSession sesion = request.getSession();
    	if (ventaId != null) {
    		Venta venta = venService.findByIdVenta(ventaId);
            if (venta != null) {
            	try {
            		Producto producto = venta.getProducto();
            		producto.setStock(producto.getStock() +1);
            		proService.save(producto);
            		venService.delete(ventaId);
        		}catch(Exception e) {
        			sesion.setAttribute("UsedAsForeignRegister",true);
        		}
            }
    	}
    	
    	return "ventas";
    }
    
    @RequestMapping("/DeleteVenta")
    public String DeleteVenta(HttpServletRequest request) {
    	Long ventaId = Long.parseLong(request.getParameter("inputId"));
    	HttpSession sesion = request.getSession();
    	if (ventaId != null) {
    		Venta venta = venService.findByIdVenta(ventaId);
            if (venta != null) {
            	try {
            		if(request.getParameter("reStock")!=null && request.getParameter("reStock").equals("on")) {
            			Producto producto = venta.getProducto();
            			producto.setStock(producto.getStock() +1);
            			proService.save(producto);
            		}
            		venService.delete(ventaId);
        		}catch(Exception e) {
        			sesion.setAttribute("UsedAsForeignRegister",true);
        		}
            }
    	}
    	
    	return "ventas";
    }
    
    @RequestMapping("/DeleteProducto")
    public String DeleteProducto(HttpServletRequest request) {
    	Long productoId = Long.parseLong(request.getParameter("inputId"));
    	HttpSession sesion = request.getSession();
    	if (productoId != null) {
    		Producto producto = proService.findByIdProducto(productoId);
            if (producto != null) {
            	try {
            		proService.delete(productoId);
        		}catch(Exception e) {
        			sesion.setAttribute("UsedAsForeignRegister",true);
        		}
            }
    	}
    	
    	return "productos";
    }
    
    @RequestMapping("/DeleteEmpresa")
    public String DeleteEmpresa(HttpServletRequest request) {
    	Long empresaId = Long.parseLong(request.getParameter("inputId"));
    	HttpSession sesion = request.getSession();
    	if (empresaId != null) {
    		Empresa empresa = empService.findByIdEmpresa(empresaId);
            if (empresa != null) {
            	try {
            		empService.delete(empresaId);
        		}catch(Exception e) {
        			sesion.setAttribute("UsedAsForeignRegister",true);
        		}
            }
        		
    	}
    	
    	return "empresas";
    }
    
    @RequestMapping("/DeleteMarca")
    public String DeleteMarca(HttpServletRequest request) {
    	Long marcaId = Long.parseLong(request.getParameter("inputId"));
    	HttpSession sesion = request.getSession();
    	if (marcaId != null) {
        	Marca marca = marcaService.findByIdMarca(marcaId);
            if (marca != null) {
            	try {
            		marcaService.delete(marcaId);
        		}catch(Exception e) {
        			sesion.setAttribute("UsedAsForeignRegister",true);
        		}
            }
        		
    	}
    	return "marcas";
    }
    
    @RequestMapping("/DeleteUser")
    public String DeleteUser(HttpServletRequest request) {
    	Long usuarioId = Long.parseLong(request.getParameter("inputId"));
    	HttpSession sesion = request.getSession();
    	if (usuarioId != (Long)sesion.getAttribute("Id")) {
    		if (usuarioId != null) {
        		Usuario usuario = usuService.findByIdUsuario(usuarioId);
            	if (usuario != null) {
            		try {
            			usuService.delete(usuarioId);
            		}catch(Exception e) {
            			sesion.setAttribute("UsedAsForeignRegister",true);
            		}
            		
            	}
    		}
    	}
    	return "usuarios";
    }
    
    @RequestMapping("/AddProducto")
    public String AddProducto(HttpServletRequest request) {
    	if(ProductoCreationOrModification(request,false))
    		return "productos";
    	else return "crearProducto";
    }
    
    @RequestMapping("/AddEmpresa")
    public String AddEmpresa(HttpServletRequest request) {
    	if(EmpresaCreationOrModification(request,false))
    		return "empresas";
    	else return "crearEmpresa";
    }
    
    @RequestMapping("/AddMarca")
    public String AddMarca(HttpServletRequest request) {
    	if(MarcaCreationOrModification(request,false))
    		return "marcas";
    	else return "crearMarca";
    }
    
    @RequestMapping("/AddUser")
    public String AddUser(HttpServletRequest request) {
    	if(UserCreationOrModification(request,false,false))
    		return "usuarios";
    	else return "crearUsuario";
    }
    
    @RequestMapping("/RegisterUser")
    public String RegisterUser(HttpServletRequest request) {
    	UserCreationOrModification(request,false,true);
    	return "index";
    	
    }
    
    @RequestMapping("/EditProducto")
    public String EditProducto(HttpServletRequest request) {
    	if(ProductoCreationOrModification(request,true))
    		return "productos";
    	else return "editarProducto";
    	}
    
    @RequestMapping("/EditEmpresa")
    public String EditEmpresa(HttpServletRequest request) {
    	if(EmpresaCreationOrModification(request,true))
    		return "empresas";
    	else return "editarEmpresa";
    	}
    
    @RequestMapping("/EditMarca")
    public String EditMarca(HttpServletRequest request) {
    	if(MarcaCreationOrModification(request,true))
    		return "marcas";
    	else return "editarMarca";
    	}
    
    @RequestMapping("/EditUser")
    public String EditUser(HttpServletRequest request) {
    	if(UserCreationOrModification(request,true,false)) {
    		HttpSession sesion = request.getSession();
    		System.out.println((Long)sesion.getAttribute("Id"));
    		System.out.println((Long)sesion.getAttribute("editId"));
    		if((Long)sesion.getAttribute("Id") == (Long)sesion.getAttribute("editId"))
    			sesion.setAttribute("ObsoleteSession", true);
    		return "usuarios";
    	}
    	else return "editarUsuario";
    	}
    
    
    //LOGIN
    
    @RequestMapping("/LoginController")
    public void LoginController(HttpServletRequest request, HttpServletResponse response) throws IOException {
    	
    	//QUERY SPACE
    	boolean rightPw = false;
    	String thisLogin = "";
    	String thisPw = "";
    	String inputEncryptedPw = "";
    	
    	try {
			Class.forName(driverName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		
		try {
			connection = DriverManager.getConnection(connectionUrl, userId, password);
			statement = connection.createStatement();
			String sql = "SELECT * FROM usuarios";
			resultSet = statement.executeQuery(sql);
			
			while (resultSet.next()) {
				thisLogin = resultSet.getString("login");
				thisPw = resultSet.getString("pw");
				if(request.getParameter("inputLogin").equals(thisLogin)) {
					resultSet.close();
					sql = "SELECT TO_BASE64(AES_ENCRYPT(\""+request.getParameter("inputPw") +"\",\""+ key +"\")) AS col;";
					//sql = "SELECT AES_ENCRYPT(\""+request.getParameter("inputPw") +"\",\""+ key +"\") AS col;";
					resultSet = statement.executeQuery(sql);
					while(resultSet.next()) {
						inputEncryptedPw = resultSet.getString("col");
					}
					
					if(thisPw.equals(inputEncryptedPw))
						rightPw=true;
					
				}
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
    	
    	//END OF QUERY SPACE
    	
    	Usuario usuario = usuService.findByLogin(request.getParameter("inputLogin"));
		//String pw = request.getParameter("inputPw");
		HttpSession sesion = request.getSession();
		//if(usuario!=null && pw.equals(usuario.getPw())) {
		if(usuario!=null && rightPw) {
				triedAccessWithNoLogin=false;
				wrongPassword=false;
				sesion.setAttribute("Usuario",usuario);
				sesion.setAttribute("username", usuario.getName());
				sesion.setAttribute("Acceso", usuario.getAcceso());
				sesion.setAttribute("Id", usuario.getId());
				sesion.setAttribute("EditNull", false);
				response.sendRedirect("inicio");
		}else {
			wrongPassword=true;
			response.sendRedirect("index");
		}
			 
		
    }
    
    @RequestMapping("/errorLogin")
    public String errorLogin() {

        return "error";
    }
    
    @RequestMapping("/deshabilitado")
    public String deshabilitado() {

        return "deshabilitado";
    }
}