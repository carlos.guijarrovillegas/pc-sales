package es.cguijarro.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import es.cguijarro.model.Usuario;
import es.cguijarro.repo.ConfirmationTokenRepository;
import es.cguijarro.repo.UsuarioRepository;
import es.cguijarro.services.UsuarioService;

@Controller
public class UsuarioController {
	
	@Autowired
	private UsuarioService us;
	
	
	
	
	@GetMapping("/allUsers")
	public ResponseEntity<List<Usuario>> getUsuarios() {
		List<Usuario> listaUsuarios = us.findAll();
		/*if(listaUsuarios.isEmpty()){
			//return ResponseEntity.noContent().build();
			return null;
			}
			else{
				return ResponseEntity.status(HttpStatus.OK).body(listaUsuarios);
			}*/
		return ResponseEntity.status(HttpStatus.OK).body(listaUsuarios);
	}
	
	@GetMapping("/userById/{id}")
	public ResponseEntity<Usuario> findById(@PathVariable("id") Long id) {
		Usuario user = us.findByIdUsuario(id);
		if(user==null){
			return ResponseEntity.notFound().build();
			}
			else{
			return ResponseEntity.ok(user);
			}
	}
	
	@GetMapping("/userByLogin/{login}")
	public ResponseEntity<Usuario> findByLogin(@PathVariable("login") String login) {
		Usuario user = us.findByLogin(login);
		if(user==null){
			return ResponseEntity.notFound().build();
			}
			else{
			return ResponseEntity.ok(user);
			}
	}
	
	
	
//	@PostMapping("/createUser")
//	public ResponseEntity<Usuario> createUser(@RequestBody Usuario usuario){
//		Usuario nuevoUsuario = us.save(usuario);
//		return new ResponseEntity<>(nuevoUsuario,HttpStatus.CREATED);
//	}
	
}
