package es.cguijarro.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cguijarro.model.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, Long> {
	Producto findByNombre(String nombre);
	Producto findByMarca(String marca);
	Producto findByEmpresa(String empresa);
}
