package es.cguijarro.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import es.cguijarro.security.ConfirmationToken;

public interface ConfirmationTokenRepository extends JpaRepository<ConfirmationToken, String> {
    ConfirmationToken findByConfirmationToken(String confirmationToken);
}