package es.cguijarro.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cguijarro.model.Venta;

@Repository
public interface VentaRepository extends JpaRepository<Venta, Long> {
	
}
