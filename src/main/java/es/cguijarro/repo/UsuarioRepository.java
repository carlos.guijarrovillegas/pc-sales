package es.cguijarro.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cguijarro.model.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	Usuario findByName(String nombre);
	Usuario findByLogin(String login);
    //Usuario findByEmailIdIgnoreCase(String emailId);
}
