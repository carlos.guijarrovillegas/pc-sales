package es.cguijarro.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import es.cguijarro.model.Marca;

@Repository
public interface MarcaRepository extends JpaRepository<Marca, Long> {
	Marca findByNombre(String nombre);
}
