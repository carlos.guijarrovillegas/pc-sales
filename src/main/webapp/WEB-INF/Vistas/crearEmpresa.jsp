<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="Maestras/adminChecker.jsp" %>
<%@page import="org.json.simple.JSONObject"%>
<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<body>
	<%@include file="Maestras/header.jsp" %>
	<%@include file="Maestras/session.jsp" %>
	<%@include file="Maestras/navBar.jsp" %>
	<%@include file="Maestras/dbConnection.jsp" %>
	
	<div class=content>
	<h2>Crear una nueva empresa manualmente</h2>
	<p>Esta acción solo puede ser llevada a cabo por un administrador.</p>
	
	<%
					
					if(session.getAttribute("WentWrong") != null && (boolean)session.getAttribute("WentWrong")==true){
						out.print("<p class=error>La empresa no se ha creado correctamente. Solucione los errores para continuar</p>");
						session.setAttribute("WentWrong",null);	
					}
					%>
					<form action="AddEmpresa" method="post">
						<label class="small" for="inputNombre">Nombre</label>
						<input class="autoMargin" id="inputNombre" name="inputNombre" type="text" placeholder="Nombre de empresa..."  required maxlength="30"/><br>
						<%
						
						
						if(session.getAttribute("UsedNombre") != null && (boolean)session.getAttribute("UsedNombre")){
							out.print("<p class=error>¡La empresa "+session.getAttribute("UsedNombreString") +" ya existe!</p>");
							session.setAttribute("UsedNombre",null);
							
						}
						
						if(session.getAttribute("NombreTooLong") != null && (boolean)session.getAttribute("NombreTooLong")==true){
							out.print("<p class=error>Este nombre es demasiado largo; no puede sobrepasar los 30 caracteres</p>");
							session.setAttribute("NombreTooLong",null);	
						}
							%>
							
						<label class="small" for="inputProvincia">Provincia</label>
						<input class="autoMargin" id="inputProvincia" name="inputProvincia" type="text" placeholder="EJ. Zaragoza"  required maxlength="30"/><br>
						<%
						if(session.getAttribute("ProvinciaTooLong") != null && (boolean)session.getAttribute("ProvinciaTooLong")==true){
							out.print("<p class=error>Este nombre de provincia es demasiado largo; no puede sobrepasar los 30 caracteres</p>");
							session.setAttribute("ProvinciaTooLong",null);	
						}
						%>
						<label class="small" for="inputWeb">Sitio web</label>
						<input class="autoMargin" id="inputWeb" name="inputWeb" type="text" placeholder="Sitio web..."  required maxlength="100"/><br>
						<%
						if(session.getAttribute("WrongWeb") != null && (boolean)session.getAttribute("WrongWeb")==true){
							out.print("<p class=error>Introduce un sitio web válido</p>");
							session.setAttribute("WrongWeb",null);	
						}
						%>
						<input type="submit" class="btn myBtn" value="Crear empresa" />
					</form>
	</div>
</body>
</html>