<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="Maestras/adminChecker.jsp" %>
<%@page import="org.json.simple.JSONObject"%>
<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<body>
	<%@include file="Maestras/header.jsp" %>
	<%@include file="Maestras/session.jsp" %>
	<%@include file="Maestras/navBar.jsp" %>
	<%@include file="Maestras/dbConnection.jsp" %>
	
	<div class=content>
	<h2>Editar un producto</h2>
	<p>Esta acción solo puede ser llevada a cabo por un administrador.</p>
	
	<%
	
	//BD SETUP
	try {
		Class.forName(driverName);
	} catch (ClassNotFoundException e) {
		e.printStackTrace();
	}
	
	Connection connection = null;
	Statement statement = null;
	ResultSet resultSet = null;
	//END OF BD SETUP
					
					if(session.getAttribute("WentWrong") != null && (boolean)session.getAttribute("WentWrong")==true){
						out.print("<p class=error>El producto no se ha creado correctamente. Solucione los errores para continuar</p>");
						session.setAttribute("WentWrong",null);	
					}
					%>
					<form action="EditProducto" method="post">
						<label class="small" for="inputMarca">Marca</label>
						<select class="autoMargin" id="inputMarca" name="inputMarca">
							<%
							try{
								connection = DriverManager.getConnection(connectionUrl, userId, password);
								statement = connection.createStatement();
								String sql = "SELECT id, nombre FROM marcas;";
								resultSet = statement.executeQuery(sql);
								int rows = 0;
								
								while (resultSet.next()) {
									out.print("<option value='"+resultSet.getString("id")+"' " );
									if((Long)session.getAttribute("editMarcaId") == Long.parseLong(resultSet.getString("id")))
										out.print("selected='yes'");
									out.print(" >"+resultSet.getString("nombre")+"</option>");
									rows++;
								}
								if(rows==0)
									out.print("<p class=error>No se ha encontrado ninguna marca disponible. Vaya a la tabla Marcas y cree una antes de continuar</p>");
							}
							catch (Exception e) {
								e.printStackTrace();
							}
							%>
						</select><br/>
						
						<%
						
						
						if(session.getAttribute("WrongMarca") != null && (boolean)session.getAttribute("WrongMarca")){
							out.print("<p class=error>La marca introducida no es correcta</p>");
							session.setAttribute("WrongMarca",null);
							
						} 
						
						%>
						
						<label class="small" for="inputNombre">Nombre</label>
						<input class="autoMargin" id="inputNombre" name="inputNombre" type="text" placeholder="Nombre de producto..."  required maxlength="30" <%
		out.print("value='"+session.getAttribute("editNombre")+"'");%>/><br>
						<%
						
						
						if(session.getAttribute("UsedNombre") != null && (boolean)session.getAttribute("UsedNombre")){
							out.print("<p class=error>¡El producto "+session.getAttribute("UsedNombreString") +" ya está siendo vendido por otra empresa o pertenece a otra marca!</p>");
							session.setAttribute("UsedNombre",null);
							
						}
						
						if(session.getAttribute("NombreTooLong") != null && (boolean)session.getAttribute("NombreTooLong")==true){
							out.print("<p class=error>Este nombre es demasiado largo; no puede sobrepasar los 50 caracteres</p>");
							session.setAttribute("NombreTooLong",null);	
						}
							%>
							
						<label class="small" for="inputPrecio">Precio</label>
						<input class="autoMargin" id="inputPrecio" name="inputPrecio" type="text" placeholder="EJ. 39.95"  required maxlength="30" <%
		out.print("value='"+session.getAttribute("editPrecio")+"'");%>/> €<br>
						<%
						if(session.getAttribute("WrongPrecio") != null && (boolean)session.getAttribute("WrongPrecio")==true){
							out.print("<p class=error>El precio introducido no parece ser correcto. Introduzca un valor válido</p>");
							session.setAttribute("WrongPrecio",null);	
						}
						
						if(session.getAttribute("PriceTooLowOrHigh") != null && (boolean)session.getAttribute("PriceTooLowOrHigh")==true){
							out.print("<p class=error>El precio debe estar entre 0.01 y 5000 €</p>");
							session.setAttribute("PriceTooLowOrHigh",null);	
						}
						%>
						<label class="small" for="inputStock">Unidades en Stock</label>
						<input class="autoMargin" id="inputStock" name="inputStock" type="number" min="0" max="100"  required <%
		out.print("value='"+session.getAttribute("editStock")+"'");%>/><br>
						<%
						if(session.getAttribute("StockTooLowOrHigh") != null && (boolean)session.getAttribute("StockTooLowOrHigh")==true){
							out.print("<p class=error>El stock debe ser de entre 0 y 100</p>");
							session.setAttribute("StockTooLowOrHigh",null);	
						}
						
						if(session.getAttribute("WrongStock") != null && (boolean)session.getAttribute("WrongStock")==true){
							out.print("<p class=error>Introduce un número de stock válido</p>");
							session.setAttribute("WrongStock",null);	
						}
						%>
						
						<label class="small" for="inputEmpresa">Empresa vendedora</label>
						<select class="autoMargin" id="inputEmpresa" name="inputEmpresa">
							<%
							try{
								connection = DriverManager.getConnection(connectionUrl, userId, password);
								statement = connection.createStatement();
								String sql = "SELECT id, nombre FROM empresas;";
								resultSet = statement.executeQuery(sql);
								int rows = 0;
								
								while (resultSet.next()) {
									out.print("<option value='"+resultSet.getString("id")+"' " );
									if((Long)session.getAttribute("editEmpresaId") == Long.parseLong(resultSet.getString("id")))
										out.print("selected='yes'");
									out.print(" >"+resultSet.getString("nombre")+"</option>");
									rows++;
								}
								if(rows==0)
									out.print("<p class=error>No se ha encontrado ninguna empresa disponible. Vaya a la tabla Empresas y cree una antes de continuar</p>");
							}
							catch (Exception e) {
								e.printStackTrace();
							}
							%>
						</select>
						
						
						
						<input type="submit" class="btn myBtn" value="Editar producto" />
					</form>
	</div>
</body>
</html>