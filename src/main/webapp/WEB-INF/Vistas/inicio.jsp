<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<%@include file="Maestras/loginChecker.jsp" %>
<body>
	<%@include file="Maestras/header.jsp" %>
	<%@include file="Maestras/session.jsp" %>
	<%@include file="Maestras/navBar.jsp" %>
	<div class=content>
	<h1>Inicio</h1>
	<p>A continuación puede usar la barra de navegación para cambiar entre las distintas tablas disponibles.</p>
	<%
		if((Byte)session.getAttribute("Acceso") == 1){
			out.print("<p>Al ser usted administrador, tiene permisos para:</p>");
			out.print("<ul>");
			out.print("<li>Visualizar el campo ID de cada registro</li>");
			out.print("<li>Acceder a las tablas Usuarios y Ventas</li>");
			out.print("<li>Editar y eliminar los registros en todas las tablas</li>");
			out.print("</ul>");
		}
	%>
	</div>
</body>
</html>