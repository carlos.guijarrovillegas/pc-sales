<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
if(session.getAttribute("username") == null || (Byte)session.getAttribute("Acceso") != 0)
	response.sendRedirect("index");

%>
<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<body>
<%@include file="Maestras/header.jsp" %>
	<div class=content>
		<h2>¡Lo sentimos!</h2>
		<h3>Su usuario está <b>deshabilitado</b> actualmente. Debe esperar a que un administrador le de acceso</h3>
		<form name="returnToIndexForm" method="POST" action="index">
			<input type="submit" value="Volver a inicio" class="btn myBtn">
		</form>
	</div>
</body>
</html>