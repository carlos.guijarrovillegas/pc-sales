<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="Maestras/loginChecker.jsp" %>
<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<body>
	<%@include file="Maestras/header.jsp" %>
	<%@include file="Maestras/session.jsp" %>
	<%@include file="Maestras/navBar.jsp" %>
	<%@include file="Maestras/dbConnection.jsp" %>
	<div class=content>
	<form name="searchForm" method="POST" action="SearchProducto">
		<input id='inputSearch' name='inputSearch' type='text' placeholder='Buscar...' onchange='this.form.submit()' <%
		if(session.getAttribute("busqueda")!=null)
			out.print("value='"+session.getAttribute("busqueda")+"'");
		%> >
	</form>
		<h2>Lista de productos</h2>
		<%
			try {
				Class.forName(driverName);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			
			Connection connection = null;
			Statement statement = null;
			ResultSet resultSet = null;
			

			if(session.getAttribute("Success") != null && (boolean)session.getAttribute("Success")==true){
				out.print("<p class=success>La operación se ha realizado con éxito</p>");
				session.setAttribute("Success",false);
				session.setAttribute("Success",null);	
			}
			if(session.getAttribute("UsedAsForeignRegister") != null && (boolean)session.getAttribute("UsedAsForeignRegister")==true){
				out.print("<p class=error>No puedes borrar este producto ahora mismo, ya que aparece relacionado a algunos registros en Ventas</p>");
				session.setAttribute("UsedAsForeignRegister",false);
				session.setAttribute("UsedAsForeignRegister",null);	
			}
			
			if(session.getAttribute("SuccessBuy") != null && (boolean)session.getAttribute("SuccessBuy")==true){
				out.print("<p class=success>Has comprado un "+session.getAttribute("BuyMarcaName")+" "+session.getAttribute("BuyProductoName")+" por "+session.getAttribute("BuyPrecio")+" €.</p>");
				session.setAttribute("SuccessBuy",false);
				session.setAttribute("SuccessBuy",null);	
			}else if(session.getAttribute("SuccessBuy") != null && (boolean)session.getAttribute("SuccessBuy")==false){
				out.print("<p class=error>No se puede comprar un producto si no queda stock del mismo</p>");
				session.setAttribute("SuccessBuy",false);
				session.setAttribute("SuccessBuy",null);
			}
			
			%>
			<table>
			 <%
			 out.print("<tr>");
			 if((Byte)session.getAttribute("Acceso") ==1){
				 out.print("<th>Acciones</th>");
				 out.print("<th>ID</th>");
			 }
			 out.print("<th>Marca</th>");
			 out.print("<th>Nombre</th>");
			 out.print("<th>Precio</th>");
			 out.print("<th>Unidades en stock</th>");
			 out.print("<th>Vendido por</th>");
			 out.print("</tr>");
			try {
				connection = DriverManager.getConnection(connectionUrl, userId, password);
				statement = connection.createStatement();
				String sql;
				if(session.getAttribute("busqueda")!=null && !session.getAttribute("busqueda").equals(""))
					sql = "SELECT p.id, m.nombre, p.nombre, cast(p.precio as decimal(10,2)) precio, p.stock, e.nombre FROM productos p JOIN marcas m ON p.marca_id = m.id JOIN empresas e ON p.empresa_id=e.id WHERE m.nombre LIKE '%"+session.getAttribute("busqueda")+"%' OR p.nombre LIKE '%"+session.getAttribute("busqueda")+"%' OR precio LIKE '%"+session.getAttribute("busqueda")+"%' OR p.stock LIKE '%"+session.getAttribute("busqueda")+"%' OR e.nombre LIKE '%"+session.getAttribute("busqueda")+"%'  ORDER BY m.nombre ASC;";
				else
					sql = "SELECT p.id, m.nombre, p.nombre, cast(p.precio as decimal(10,2)) precio, p.stock, e.nombre FROM productos p JOIN marcas m ON p.marca_id = m.id JOIN empresas e ON p.empresa_id=e.id ORDER BY m.nombre ASC;";
				resultSet = statement.executeQuery(sql);
				int rows = 0;
				while (resultSet.next()) {
					out.print("<tr>");
					if((Byte)session.getAttribute("Acceso") ==1){
						out.print("<td>");
						out.print("<form name='editForm' method='POST' action='EditarProducto'>");
						out.print("<input id='editButton' name='editButton' type='submit' class='' value='Editar'>");
						out.print("<input id='inputIdEdit' name='inputIdEdit' type='number' value='"+resultSet.getString("p.id")+"' hidden>");
						out.print("</form>");
						out.print("<form name='deleteForm' method='POST' action='DeleteProducto' onSubmit=\"return confirm('¿Seguro que deseas eliminar el producto "+ resultSet.getString("m.nombre") + " " + resultSet.getString("p.nombre") + "?') \">");
						out.print("<input id='deleteButton' name='deleteButton' type='submit' class='' value='Eliminar'>");
						out.print("<input id='inputId' name='inputId' type='number' value='"+resultSet.getString("p.id")+"' hidden>");
						out.print("</form>");
						out.print("</td>");
						out.print("<td>"+resultSet.getString("p.id")+"</td>");
					}
					out.print("<td>"+resultSet.getString("m.nombre")+"</td>");
					out.print("<td>"+resultSet.getString("p.nombre")+"</td>");
					out.print("<td>"+resultSet.getString("precio")+" €</td>");
					if(resultSet.getString("p.stock").equals("0"))
						out.print("<td style='color: red;'>0</td>");
					else out.print("<td>"+resultSet.getString("p.stock")+"</td>");
					out.print("<td>"+resultSet.getString("e.nombre")+"</td>");
					out.print("<td>");
					out.print("<form name='comprarForm' method='POST' action='ComprarProducto' onSubmit=\"return confirm('¿Seguro que deseas comprar el producto "+ resultSet.getString("m.nombre") + " " + resultSet.getString("p.nombre") + " por " + resultSet.getString("precio") + " €?') \">");
					out.print("<input id='comprarButton' name='comprarButton' type='submit' class='btn myBtn' value='Comprar' ");
					if(resultSet.getString("p.stock").equals("0"))
						out.print("disabled style='background-color: red;'");
					out.print(">");
					out.print("<input id='inputIdComprar' name='inputIdComprar' type='number' value='"+resultSet.getString("p.id")+"' hidden>");
					out.print("</form>");
					out.print("</td>");
					out.print("</tr>");
					rows++;
				}
				if(rows==0)
					out.print("<p class=error>No se han encontrado registros</p>");
			} catch (Exception e) {
					e.printStackTrace();
			}
			%>
		</table>
		<hr>
		<%
		if((Byte)session.getAttribute("Acceso") ==1){
			 out.print("<form name='crearProductoForm' method='POST' action='CrearProducto'>");
			 out.print("<input id='crearProductoButton' name='crearProductoButton' type='submit' class='btn myBtn' value='Añadir producto'>");
			 out.print("</form>");
		 }
		%>
	</div>
</body>
</html>