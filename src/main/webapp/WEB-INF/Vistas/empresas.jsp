<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="Maestras/loginChecker.jsp" %>
<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<body>
	<%@include file="Maestras/header.jsp" %>
	<%@include file="Maestras/session.jsp" %>
	<%@include file="Maestras/navBar.jsp" %>
	<%@include file="Maestras/dbConnection.jsp" %>
	<div class=content>
	<form name="searchForm" method="POST" action="SearchEmpresa">
		<input id='inputSearch' name='inputSearch' type='text' placeholder='Buscar...' onchange='this.form.submit()' <%
		if(session.getAttribute("busqueda")!=null)
			out.print("value='"+session.getAttribute("busqueda")+"'");
		%> >
	</form>
		<h2>Lista de empresas</h2>
		<%
			try {
				Class.forName(driverName);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			
			Connection connection = null;
			Statement statement = null;
			ResultSet resultSet = null;
			

			if(session.getAttribute("Success") != null && (boolean)session.getAttribute("Success")==true){
				out.print("<p class=success>La operación se ha realizado con éxito</p>");
				session.setAttribute("Success",false);
				session.setAttribute("Success",null);	
			}
			if(session.getAttribute("UsedAsForeignRegister") != null && (boolean)session.getAttribute("UsedAsForeignRegister")==true){
				out.print("<p class=error>No puedes borrar esta empresa ahora mismo, ya que aparece relacionado a algunos registros en Productos</p>");
				session.setAttribute("UsedAsForeignRegister",false);
				session.setAttribute("UsedAsForeignRegister",null);	
			}
			
			%>
			<table>
			 <%
			 out.print("<tr>");
			 if((Byte)session.getAttribute("Acceso") ==1){
				 out.print("<th>Acciones</th>");
				 out.print("<th>ID</th>");
			 }
			 out.print("<th>Nombre</th>");
			 out.print("<th>Provincia</th>");
			 out.print("<th>Web</th>");
			 out.print("</tr>");
			try {
				connection = DriverManager.getConnection(connectionUrl, userId, password);
				statement = connection.createStatement();
				String sql;
				if(session.getAttribute("busqueda")!=null && !session.getAttribute("busqueda").equals(""))
					sql = "SELECT * FROM empresas WHERE nombre LIKE '%"+session.getAttribute("busqueda")+"%' OR provincia LIKE '%"+session.getAttribute("busqueda")+"%' OR web LIKE '%"+session.getAttribute("busqueda")+"%' ORDER BY nombre ASC";
				else
					sql = "SELECT * FROM empresas ORDER BY nombre ASC";
				resultSet = statement.executeQuery(sql);
				int rows = 0;
				while (resultSet.next()) {
					out.print("<tr>");
					if((Byte)session.getAttribute("Acceso") ==1){
						out.print("<td>");
						out.print("<form name='editForm' method='POST' action='EditarEmpresa'>");
						out.print("<input id='editButton' name='editButton' type='submit' class='' value='Editar'>");
						out.print("<input id='inputIdEdit' name='inputIdEdit' type='number' value='"+resultSet.getString("id")+"' hidden>");
						out.print("</form>");
						out.print("<form name='deleteForm' method='POST' action='DeleteEmpresa' onSubmit=\"return confirm('¿Seguro que deseas eliminar la empresa "+ resultSet.getString("nombre") + "?') \">");
						out.print("<input id='deleteButton' name='deleteButton' type='submit' class='' value='Eliminar'>");
						out.print("<input id='inputId' name='inputId' type='number' value='"+resultSet.getString("id")+"' hidden>");
						out.print("</form>");
						out.print("</td>");
						out.print("<td>"+resultSet.getString("id")+"</td>");
					}
					out.print("<td>"+resultSet.getString("nombre")+"</td>");
					out.print("<td>"+resultSet.getString("provincia")+"</td>");
					out.print("<td><a href='http://"+resultSet.getString("web")+"' target=\"_blank\">"+resultSet.getString("web")+"</a></td>");
					out.print("</tr>");
					rows++;
				}
				if(rows==0)
					out.print("<p class=error>No se han encontrado registros</p>");
			} catch (Exception e) {
					e.printStackTrace();
			}
			%>
		</table>
		<hr>
		<%
		if((Byte)session.getAttribute("Acceso") ==1){
			 out.print("<form name='crearEmpresaForm' method='POST' action='CrearEmpresa'>");
			 out.print("<input id='crearEmpresaButton' name='crearEmpresaButton' type='submit' class='btn myBtn' value='Añadir empresa'>");
			 out.print("</form>");
		 }
		%>
	</div>
</body>
</html>