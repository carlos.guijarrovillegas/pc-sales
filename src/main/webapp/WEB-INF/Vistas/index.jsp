<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<body>
	<%@include file="Maestras/header.jsp" %>
	<div class=content>
		<div class=login>
			<div class=row>
				<div class=col-md-2>
					<h3>Inicio de sesión</h3>
					<form action="LoginController" method="post">
						<label class="small" for="inputLogin">Usuario</label>
						<input class="autoMargin" id="inputLogin" name="inputLogin" type="text" placeholder="Nombre de usuario..." /><br>
						<label class="small" for="inputPw">Contraseña</label>
						<input class="autoMargin" id="inputPw" name="inputPw" type="password" placeholder="Contraseña..." />
						<input type="submit" class="btn myBtn" value="Iniciar sesión" />
					</form>
				</div>
				<div class=col-md-2>
						
						<%
						if(session.getAttribute("AccessWithNoLogin")!=null){
							if((boolean)session.getAttribute("AccessWithNoLogin"))
								out.print("<p class=error>¡No puedes acceder al sitio sin antes iniciar sesión!</p>");
						} 
						
						if(session.getAttribute("WrongPassword")!=null){
							if((boolean)session.getAttribute("WrongPassword"))
								out.print("<p class=error>Usuario o contraseña incorrectos</p>");
						}
						
						if(session.getAttribute("EditNull")!=null){
							if((boolean)session.getAttribute("EditNull"))
								out.print("<p class=error>¡No puedes acceder a la página de edición de usuario sin antes seleccionar uno!</p>");
						}
						%>
				</div>
				<div class=col-md-2>
					<h3>Registro</h3>
					<%
					if(session.getAttribute("Success") != null && (boolean)session.getAttribute("Success")==true){
						out.print("<p class=success>El usuario se ha creado con éxito. Inicie sesión para continuar</p>");
						session.setAttribute("Success",null);	
					}
					
					if(session.getAttribute("WentWrong") != null && (boolean)session.getAttribute("WentWrong")==true){
						out.print("<p class=error>El usuario no se ha creado correctamente. Solucione los errores para continuar</p>");
						session.setAttribute("WentWrong",null);	
					}
					%>
					<form action="RegisterUser" method="post">
						<label class="small" for="inputLogin">Usuario</label>
						<input class="autoMargin" id="inputLogin" name="inputLogin" type="text" placeholder="Nombre de usuario..."  required maxlength="10"/><br>
						<%
						
						
						if(session.getAttribute("UsedLogin") != null && (boolean)session.getAttribute("UsedLogin")){
							out.print("<p class=error>¡El usuario "+session.getAttribute("UsedLoginString") +" ya existe!</p>");
							session.setAttribute("UsedLogin",null);
							
						}
						
						if(session.getAttribute("LoginTooLong") != null && (boolean)session.getAttribute("LoginTooLong")==true){
							out.print("<p class=error>Este nombre de usuario es demasiado largo; no puede sobrepasar los 10 caracteres</p>");
							session.setAttribute("LoginTooLong",null);	
						}
							%>
							
						<label class="small" for="inputName">Nombre</label>
						<input class="autoMargin" id="inputName" name="inputName" type="text" placeholder="EJ. Daniel Gómez"  required maxlength="50"/><br>
						<%
						if(session.getAttribute("NameTooLong") != null && (boolean)session.getAttribute("NameTooLong")==true){
							out.print("<p class=error>Este nombre es demasiado largo; no puede sobrepasar los 50 caracteres</p>");
							session.setAttribute("NameTooLong",null);	
						}
						%>
						<label class="small" for="inputPw">Contraseña</label>
						<input class="autoMargin" id="inputPw" name="inputPw" type="password" placeholder="Contraseña..."  required maxlength="30"/><br>
						<%
						if(session.getAttribute("WrongPasswordSize") != null && (boolean)session.getAttribute("WrongPasswordSize")==true){
							out.print("<p class=error>La contraseña debe tener entre 6 y 30 caracteres</p>");
							session.setAttribute("WrongPasswordSize",null);	
						}
						%>
						<label class="small" for="inputPwRepeat">Repite la contraseña</label>
						<input class="autoMargin" id="inputPwRepeat" name="inputPwRepeat" type="password" placeholder="Contraseña..."  required maxlength="30"/><br>
						<%
						if(session.getAttribute("PasswordDoesntMatch") != null && (boolean)session.getAttribute("PasswordDoesntMatch")==true){
							out.print("<p class=error>Las contraseñas no coinciden</p>");
							session.setAttribute("PasswordDoesntMatch",null);	
						}
						%>
						<label class="small" for="inputEmail">Correo electrónico</label>
						<input class="autoMargin" id="inputEmail" name="inputEmail" type="text" placeholder="Correo electrónico..."  required maxlength="50"/><br>
						<%
						if(session.getAttribute("WrongEmailSyntax") != null && (boolean)session.getAttribute("WrongEmailSyntax")==true){
							out.print("<p class=error>Introduce un correo electrónico válido</p>");
							session.setAttribute("WrongEmailSyntax",null);	
						}
						%>
						<label class="small" for="inputTlf">Nº de teléfono</label>
						<input class="autoMargin" id="inputTlf" name="inputTlf" type="number" min="600000000" max="999999999"  required/><br>
						<%
						if(session.getAttribute("WrongTlfNumber") != null && (boolean)session.getAttribute("WrongTlfNumber")==true){
							out.print("<p class=error>Introduce un número de teléfono válido</p>");
							session.setAttribute("WrongTlfNumber",null);	
						}
						%>
						<input type="submit" class="btn myBtn" value="Registrarse" />
					</form>
				</div>
			</div>
		</div>
	</div>




<!-- JS -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>