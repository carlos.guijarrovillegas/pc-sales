<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="Maestras/loginChecker.jsp" %>
<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<body>
	<%@include file="Maestras/header.jsp" %>
	<%@include file="Maestras/session.jsp" %>
	<%@include file="Maestras/navBar.jsp" %>
	<%@include file="Maestras/dbConnection.jsp" %>
	<div class=content>
	<form name="searchForm" method="POST" action="SearchVenta">
		<input id='inputSearch' name='inputSearch' type='text' placeholder='Buscar...' onchange='this.form.submit()' <%
		if(session.getAttribute("busqueda")!=null)
			out.print("value='"+session.getAttribute("busqueda")+"'");
		%> >
	</form>
		<h2>Lista de ventas</h2>
		<%
			try {
				Class.forName(driverName);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			
			Connection connection = null;
			Statement statement = null;
			ResultSet resultSet = null;
			

			if(session.getAttribute("Success") != null && (boolean)session.getAttribute("Success")==true){
				out.print("<p class=success>La operación se ha realizado con éxito</p>");
				session.setAttribute("Success",false);
				session.setAttribute("Success",null);	
			}
			if(session.getAttribute("UsedAsForeignRegister") != null && (boolean)session.getAttribute("UsedAsForeignRegister")==true){
				out.print("<p class=error>No puedes borrar esta venta ahora mismo, ya que aparece relacionado a algunos registros en otras tablas</p>");
				session.setAttribute("UsedAsForeignRegister",false);
				session.setAttribute("UsedAsForeignRegister",null);	
			}
			
			%>
			<table>
			 <%
			out.print("<tr>");
			out.print("<th>Acciones</th>");
				if((Byte)session.getAttribute("Acceso") ==1){
					out.print("<th>ID</th>");
					out.print("<th>Usuario</th>");
				}
			out.print("<th>Marca</th>");
			out.print("<th>Producto</th>");
			out.print("<th>Vendido por</th>");
			out.print("</tr>");
			try {
				connection = DriverManager.getConnection(connectionUrl, userId, password);
				statement = connection.createStatement();
				String sql = "SELECT v.id AS id, u.name AS usuNombre, u.id AS usuId, p.mNombre AS marcaNombre, p.pNombre AS proNombre, p.eNombre AS empNombre FROM ventas v JOIN usuarios u ON v.usuario_id = u.id JOIN (SELECT p.id AS id, p.nombre AS pNombre, m.nombre AS mNombre, e.nombre AS eNombre FROM productos p JOIN marcas m ON p.marca_id = m.id JOIN empresas e ON p.empresa_id = e.id) p ON v.producto_id = p.id ORDER BY usuNombre, proNombre ASC";
				if(session.getAttribute("busqueda")!=null && !session.getAttribute("busqueda").equals(""))
					sql += "WHERE u.name LIKE '%"+session.getAttribute("busqueda")+"%' OR p.mNombre LIKE '%"+session.getAttribute("busqueda")+"%' OR p.pNombre LIKE '%"+session.getAttribute("busqueda")+"%' OR p.eNombre LIKE '%"+session.getAttribute("busqueda")+"%' ORDER BY usuNombre, proNombre ASC";
				resultSet = statement.executeQuery(sql);
				int rows = 0;
				while (resultSet.next()) {
					Long thisId = (Long)session.getAttribute("Id");
					if((Byte)session.getAttribute("Acceso") ==1 || Long.parseLong(resultSet.getString("usuId")) == thisId){
						out.print("<tr>");
						out.print("<td>");
						if((Byte)session.getAttribute("Acceso") ==1){
							out.print("<form name='deleteForm' method='POST' action='DeleteVenta' onSubmit=\"return confirm('¿Seguro que deseas eliminar la venta de "+ resultSet.getString("usuNombre") + "?') \">");
							out.print("<label for='reStock' class='autoMargin'>Reponer stock</label>");
							out.print("<input id='reStock' name='reStock' type='checkbox' class=''>");
							out.print("<input id='deleteButton' name='deleteButton' type='submit' class='autoMargin' value='Eliminar'>");
						}else{
							out.print("<form name='deleteForm' method='POST' action='TramitarDevolucion' onSubmit=\"return confirm('¿Seguro que deseas devolver el producto "+ resultSet.getString("proNombre") + "?') \">");
							out.print("<input id='deleteButton' name='deleteButton' type='submit' class='autoMargin' value='Tramitar devolución'>");
						}
						out.print("<input id='inputId' name='inputId' type='number' value='"+resultSet.getString("id")+"' hidden>");
						out.print("</form>");
						
						out.print("</td>");
						if((Byte)session.getAttribute("Acceso") ==1){
							out.print("<td>"+resultSet.getString("id")+"</td>");
							out.print("<td>"+resultSet.getString("usuNombre")+"</td>");
						}
						out.print("<td>"+resultSet.getString("marcaNombre")+"</td>");
						out.print("<td>"+resultSet.getString("proNombre")+"</td>");
						out.print("<td>"+resultSet.getString("empNombre")+"</td>");
						out.print("</tr>");
						rows++;
					}
				}
				if(rows==0)
					out.print("<p class=error>No se han encontrado registros</p>");
			} catch (Exception e) {
					e.printStackTrace();
			}
			%>
		</table>
		<hr>
	</div>
</body>
</html>