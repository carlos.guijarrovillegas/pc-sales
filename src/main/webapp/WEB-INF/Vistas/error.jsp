<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<body>
	<%@include file="Maestras/header.jsp" %>
	<div class=content>
	<h2>Error</h2>
	<p class=error>No se ha encontrado esta página</p>
	<form name="returnToIndexForm" method="POST" action="index">
	<p>Click <input type="submit" value="aquí"> para volver a la página principal</p>
	</form>
	</div>
</body>
</html>