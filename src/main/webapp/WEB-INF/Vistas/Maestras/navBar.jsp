<div class="content navBar">
	<ul class="nav">
        <li>
	        <form name="productosForm" method="POST" action="productos">
				<input id="productosButton" name="productosButton" type="submit" class="sectionButton" value="Productos" />
			</form>	        
        </li>
        <li>
			<form name="marcasForm" method="POST" action="marcas">
				<input id="marcasButton" name="marcasButton" type="submit" class="sectionButton" value="Marcas" />
			</form>
        </li>
        <li>
	        <form name="empresasForm" method="POST" action="empresas">
				<input id="empresasButton" name="empresasButton" type="submit" class="sectionButton" value="Empresas" />
			</form>
        </li>
        <li>
	        <form name='ventasForm' method='POST' action='ventas'>
	        	<input id='ventasButton' name='ventasButton' type='submit' class='sectionButton' value='Ventas'>
        	</form>
        </li>
        <%
        	if((Byte)session.getAttribute("Acceso") == 1){
        		out.print("<li>");
        		out.print("<form name='usuariosForm' method='POST' action='usuarios'>");
        		out.print("<input id='usuariosButton' name='usuariosButton' type='submit' class='sectionButton' value='Usuarios'>");
        		out.print("</form>");
        		out.print("</li>");
        	}
        %>
    </ul>
</div>