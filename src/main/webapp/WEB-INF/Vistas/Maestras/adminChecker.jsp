

<%
	if(session.getAttribute("username") == null)
		response.sendRedirect("index");
	else if((Byte)session.getAttribute("Acceso") == 0 && !CurrentPage(request).equals("deshabilitado.jsp"))
		response.sendRedirect("deshabilitado");
	else if( (Byte)session.getAttribute("Acceso") != 1)
		response.sendRedirect("noPermission");
	
%>