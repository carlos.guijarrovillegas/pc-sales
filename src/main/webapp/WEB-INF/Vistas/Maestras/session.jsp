<div class="content session">
		<div class=row>
			<div class=col-md>
				<h2>Bienvenido, <%=(String)session.getAttribute("username")%></h2>
			</div>
			<div class=col-md>
				<p><%
					if((Byte)session.getAttribute("Acceso") == 1)
						out.print("Administrador");
					else out.print("Usuario est�ndar");
				%></p>
			</div>
			<div class=col-md>
				<div class=row>
					<div class=col-md-3>
						<form name="logOutForm" method="POST" action="Logout">
							<input id="logOutButton" name="logOutButton" type="submit" class="btn myBtn alignRight" value="Cerrar sesi�n" />
						</form>
					</div>
					<div class=col-md-3>
						<form name="returnInicioForm" method="POST" action="inicio">
							<input id="returnInicioButton" name="returnInicioButton" type="submit" class="btn myBtn alignRight" value="Volver a Inicio" />
						</form>
					</div>
				</div>
			</div>
		</div>
</div>