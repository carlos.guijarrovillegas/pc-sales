<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="Maestras/adminChecker.jsp" %>
<% if(session.getAttribute("editId") == null)
		response.sendRedirect("index");%>
<%@page import="org.json.simple.JSONObject"%>
<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<body>
	<%@include file="Maestras/header.jsp" %>
	<%@include file="Maestras/session.jsp" %>
	<%@include file="Maestras/navBar.jsp" %>
	<%@include file="Maestras/dbConnection.jsp" %>
	
	<div class=content>
	<h2>Editar una marca</h2>
	<p>Esta acción solo puede ser llevada a cabo por un administrador.</p>
	
	<%
					
					if(session.getAttribute("WentWrong") != null && (boolean)session.getAttribute("WentWrong")==true){
						out.print("<p class=error>La marca no se ha editado correctamente. Solucione los errores para continuar</p>");
						session.setAttribute("WentWrong",null);	
					}
					%>
					<form action="EditMarca" method="post">
						<label class="small" for="inputNombre">Nombre</label>
						<input class="autoMargin" id="inputNombre" name="inputNombre" type="text" placeholder="Nombre de marca..."  required maxlength="30" <%
		out.print("value='"+session.getAttribute("editNombre")+"'");%>/><br>
						<%
						
						
						if(session.getAttribute("UsedNombre") != null && (boolean)session.getAttribute("UsedNombre")){
							out.print("<p class=error>¡La marca "+session.getAttribute("UsedNombreString") +" ya existe!</p>");
							session.setAttribute("UsedNombre",null);
							
						}
						
						if(session.getAttribute("NombreTooLong") != null && (boolean)session.getAttribute("NombreTooLong")==true){
							out.print("<p class=error>Este nombre es demasiado largo; no puede sobrepasar los 30 caracteres</p>");
							session.setAttribute("NombreTooLong",null);	
						}
							%>
							
						<label class="small" for="inputPais">País</label>
						<input class="autoMargin" id="inputPais" name="inputPais" type="text" placeholder="EJ. Francia"  required maxlength="30" <%
		out.print("value='"+session.getAttribute("editPais")+"'");%>/><br>
						<%
						if(session.getAttribute("PaisTooLong") != null && (boolean)session.getAttribute("PaisTooLong")==true){
							out.print("<p class=error>Este nombre de país es demasiado largo; no puede sobrepasar los 30 caracteres</p>");
							session.setAttribute("PaisTooLong",null);	
						}
						%>
						<label class="small" for="inputFiabilidad">Fiabilidad</label>
						<select class="autoMargin" id="inputFiabilidad" name="inputFiabilidad">
							  <option value="0" <% if((Byte)session.getAttribute("editFiabilidad") == 0) out.print("selected='yes'");%>>Baja</option>
							  <option value="1" <% if((Byte)session.getAttribute("editFiabilidad") == 1) out.print("selected='yes'");%>>Media</option>
							  <option value="2" <% if((Byte)session.getAttribute("editFiabilidad") == 2) out.print("selected='yes'");%>>Alta</option>
						</select>
						<%
						if(session.getAttribute("WrongFiabilidad") != null && (boolean)session.getAttribute("WrongFiabilidad")==true){
							out.print("<p class=error>Introduce un valor de fiabilidad válido</p>");
							session.setAttribute("WrongFiabilidad",null);	
						}
						%>
						<input type="submit" class="btn myBtn" value="Editar marca" />
					</form>
	</div>
</body>
</html>