<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="Maestras/adminChecker.jsp" %>
<% if(session.getAttribute("editId") == null)
		response.sendRedirect("index");%>
<%@page import="org.json.simple.JSONObject"%>
<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<body>
	<%@include file="Maestras/header.jsp" %>
	<%@include file="Maestras/session.jsp" %>
	<%@include file="Maestras/navBar.jsp" %>
	<%@include file="Maestras/dbConnection.jsp" %>
	
	<div class=content>
	<h2>Editar un usuario</h2>
	<p>Esta acción solo puede ser llevada a cabo por un administrador.</p>
	
	<%
					
					if(session.getAttribute("WentWrong") != null && (boolean)session.getAttribute("WentWrong")==true){
						out.print("<p class=error>El usuario no se ha editado correctamente. Solucione los errores para continuar</p>");
						session.setAttribute("WentWrong",null);	
					}
					%>
					<form action="EditUser" method="post">
						<label class="small" for="inputLogin">Usuario</label>
						<input class="autoMargin" id="inputLogin" name="inputLogin" type="text" placeholder="Nombre de usuario..."  required maxlength="10" <%
		out.print("value='"+session.getAttribute("editLogin")+"'");%>/><br>
						<%
						
						
						if(session.getAttribute("UsedLogin") != null && (boolean)session.getAttribute("UsedLogin")){
							out.print("<p class=error>¡El usuario "+session.getAttribute("UsedLoginString") +" ya existe!</p>");
							session.setAttribute("UsedLogin",null);
							
						}
						
						if(session.getAttribute("LoginTooLong") != null && (boolean)session.getAttribute("LoginTooLong")==true){
							out.print("<p class=error>Este nombre de usuario es demasiado largo; no puede sobrepasar los 10 caracteres</p>");
							session.setAttribute("LoginTooLong",null);	
						}
							%>
							
						<label class="small" for="inputName">Nombre</label>
						<input class="autoMargin" id="inputName" name="inputName" type="text" placeholder="EJ. Daniel Gómez"  required maxlength="50" <%
		out.print("value='"+session.getAttribute("editName")+"'");%>/><br>
						<%
						if(session.getAttribute("NameTooLong") != null && (boolean)session.getAttribute("NameTooLong")==true){
							out.print("<p class=error>Este nombre es demasiado largo; no puede sobrepasar los 50 caracteres</p>");
							session.setAttribute("NameTooLong",null);	
						}
						%>
						<label class="small" for="inputEmail">Correo electrónico</label>
						<input class="autoMargin" id="inputEmail" name="inputEmail" type="text" placeholder="Correo electrónico..."  required maxlength="50" <%
		out.print("value='"+session.getAttribute("editEmail")+"'");%>/><br>
						<%
						if(session.getAttribute("WrongEmailSyntax") != null && (boolean)session.getAttribute("WrongEmailSyntax")==true){
							out.print("<p class=error>Introduce un correo electrónico válido</p>");
							session.setAttribute("WrongEmailSyntax",null);	
						}
						%>
						<label class="small" for="inputTlf">Nº de teléfono</label>
						<input class="autoMargin" id="inputTlf" name="inputTlf" type="number" min="600000000" max="999999999"  required <%
		out.print("value='"+session.getAttribute("editMovil")+"'");%>/><br>
						<%
						if(session.getAttribute("WrongTlfNumber") != null && (boolean)session.getAttribute("WrongTlfNumber")==true){
							out.print("<p class=error>Introduce un número de teléfono válido</p>");
							session.setAttribute("WrongTlfNumber",null);	
						}
						%>
						<label class="small" for="inputAcceso">Acceso</label>
						<select class="autoMargin" id="inputAcceso" name="inputAcceso">
							  <option value="0" <% if((Byte)session.getAttribute("editAcceso") == 0) out.print("selected='yes'");%>>Deshabilitado</option>
							  <option value="2" <% if((Byte)session.getAttribute("editAcceso") == 2) out.print("selected='yes'");%>>Estándar</option>
							  <option value="1" <% if((Byte)session.getAttribute("editAcceso") == 1) out.print("selected='yes'");%>>Administrador</option>
						</select>
						<%
						if(session.getAttribute("WrongAccess") != null && (boolean)session.getAttribute("WrongAccess")==true){
							out.print("<p class=error>Introduce un acceso válido</p>");
							session.setAttribute("WrongAccess",null);	
						}
						%>
						<input type="submit" class="btn myBtn" value="Editar usuario" />
					</form>
	</div>
</body>
</html>