<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<body>
<%@include file="Maestras/header.jsp" %>
	<div class=content>
		<h2>¡Lo sentimos!</h2>
		<h3>Este usuario no tiene los permisos necesarios para acceder a esta sección.</h3>
		<p><a href="/inicio">Volver a inicio</a></p>
	</div>
</body>
</html>