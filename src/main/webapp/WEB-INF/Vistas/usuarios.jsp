<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@include file="Maestras/adminChecker.jsp" %>
<!DOCTYPE html>
<html>
<%@include file="Maestras/head.jsp" %>
<body>
	<%@include file="Maestras/header.jsp" %>
	<%@include file="Maestras/session.jsp" %>
	<%@include file="Maestras/navBar.jsp" %>
	<%@include file="Maestras/dbConnection.jsp" %>
	<div class=content>
	<form name="searchForm" method="POST" action="SearchUser">
		<input id='inputSearch' name='inputSearch' type='text' placeholder='Buscar...' onchange='this.form.submit()' <%
		if(session.getAttribute("busqueda")!=null)
			out.print("value='"+session.getAttribute("busqueda")+"'");
		%> >
	</form>
		<h2>Lista de usuarios</h2>
		<%
			try {
				Class.forName(driverName);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			
			Connection connection = null;
			Statement statement = null;
			ResultSet resultSet = null;
			
			if(session.getAttribute("ObsoleteSession") != null && (boolean)session.getAttribute("ObsoleteSession")==true){
				out.print("<p class=midFiability>La operación se ha realizado con éxito, pero necesita reiniciar su sesión para actualizar los datos</p>");
				session.setAttribute("ObsoleteSession",false);
				session.setAttribute("ObsoleteSession",null);	
			} else if(session.getAttribute("Success") != null && (boolean)session.getAttribute("Success")==true){
				out.print("<p class=success>La operación se ha realizado con éxito</p>");
				session.setAttribute("Success",false);
				session.setAttribute("Success",null);	
			}
			if(session.getAttribute("UsedAsForeignRegister") != null && (boolean)session.getAttribute("UsedAsForeignRegister")==true){
				out.print("<p class=error>No puedes borrar este usuario ahora mismo, ya que aparece relacionado a algunos registros en Ventas</p>");
				session.setAttribute("UsedAsForeignRegister",false);
				session.setAttribute("UsedAsForeignRegister",null);	
			}
			
			
			%>
			<table>
			 <%
			 out.print("<tr>");
			 out.print("<th>Acciones</th>");
			 out.print("<th>ID</th>");
			 out.print("<th>Login</th>");
			 out.print("<th>Nombre</th>");
			 out.print("<th>E-mail</th>");
			 out.print("<th>Móvil</th>");
			 out.print("<th>Acceso</th>");
			 out.print("<th>Contraseña (encriptada)</th>");
			 out.print("</tr>");
			try {
				connection = DriverManager.getConnection(connectionUrl, userId, password);
				statement = connection.createStatement();
				String sql;
				if(session.getAttribute("busqueda")!=null && !session.getAttribute("busqueda").equals(""))
					sql = "SELECT * FROM usuarios WHERE name LIKE '%"+session.getAttribute("busqueda")+"%' OR login LIKE '%"+session.getAttribute("busqueda")+"%' OR email LIKE '%"+session.getAttribute("busqueda")+"%' OR movil LIKE '%"+session.getAttribute("busqueda")+"%' ORDER BY login ASC";
				else
					sql = "SELECT * FROM usuarios ORDER BY login ASC";
				resultSet = statement.executeQuery(sql);
				int rows = 0;
				while (resultSet.next()) {
					out.print("<tr");
					if((Long)session.getAttribute("Id") == Long.parseLong(resultSet.getString("id")))
						out.print(" class='thisUser'");
					out.print(">");
					out.print("<td>");
					out.print("<form name='editForm' method='POST' action='EditarUsuario'>");
					out.print("<input id='editButton' name='editButton' type='submit' class='' value='Editar'>");
					out.print("<input id='inputIdEdit' name='inputIdEdit' type='number' value='"+resultSet.getString("id")+"' hidden>");
					out.print("</form>");
					out.print("<form name='deleteForm' method='POST' action='DeleteUser' onSubmit=\"return confirm('¿Seguro que deseas eliminar a "+ resultSet.getString("name") + "?') \">");
					if((Long)session.getAttribute("Id") == Long.parseLong(resultSet.getString("id")))
						out.print("<input id='deleteButton' name='deleteButton' type='submit' class='' value='Eliminar' title='¡No puede eliminarse usted mismo!' disabled>");
					else
						out.print("<input id='deleteButton' name='deleteButton' type='submit' class='' value='Eliminar'>");
					out.print("<input id='inputId' name='inputId' type='number' value='"+resultSet.getString("id")+"' hidden>");
					out.print("</form>");
					out.print("</td>");
					out.print("<td>"+resultSet.getString("id")+"</td>");
					out.print("<td>"+resultSet.getString("login")+"</td>");
					out.print("<td>"+resultSet.getString("name")+"</td>");
					out.print("<td>"+resultSet.getString("email")+"</td>");
					out.print("<td>"+resultSet.getString("movil")+"</td>");
					if(resultSet.getString("acceso").equals("0"))
						out.print("<td>Deshabilitado</td>");
					else if(resultSet.getString("acceso").equals("1"))
						out.print("<td>Administrador</td>");
					else if(resultSet.getString("acceso").equals("2"))
						out.print("<td>Estándar</td>");
					out.print("<td>"+resultSet.getString("pw")+"</td>");
					out.print("</tr>");
					rows++;
				}
				if(rows==0)
					out.print("<p class=error>No se han encontrado registros</p>");
			} catch (Exception e) {
					e.printStackTrace();
			}
			%>
		</table>
		<hr>
		<form name='crearUsuarioForm' method='POST' action='CrearUsuario'>
		<input id='crearUsuarioButton' name='crearUsuarioButton' type='submit' class='btn myBtn' value='Añadir usuario'>
		</form>
	</div>
</body>
</html>